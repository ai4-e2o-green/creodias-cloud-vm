import typing

import requests


class DataIndex(object):
    _base_url = 'https://finder.creodias.eu/resto/api/collections'

    def __init__(self,
                 collection: str,
                 platform: str,
                 processing_level: str,
                 geometry: str,
                 cloud_cover: typing.Optional[typing.Union[int, typing.List]] = None,
                 start_date: typing.Optional[str] = None,
                 max_records: int = 10) -> None:
        self.url = f'{self._base_url}/{collection}/search.json' \
                   f'?maxRecords={max_records}' \
                   f'&platform={platform}' \
                   f'&processingLevel={processing_level}' \
                   f'&sortParam=startDate' \
                   f'&sortOrder=ascending' \
                   f'&status=all' \
                   f'&dataset=ESA-DATASET'
        geometry = requests.utils.quote(geometry)
        self.url = f'{self.url}&geometry={geometry}'
        if start_date is not None:
            start_date = requests.utils.quote(start_date)
            self.url = f'{self.url}&startDate={start_date}'
        if cloud_cover is not None:
            cloud_cover = requests.utils.quote(
                f'[0,{cloud_cover}]' if isinstance(cloud_cover, str) else ','.join(cloud_cover)
            )
            self.url = f'{self.url}&cloudCover={cloud_cover}'

    def get_metadata(self) -> typing.List[typing.Optional[typing.Mapping[str, typing.Any]]]:
        res = []
        with requests.Session() as _s:
            for _feat in _s.get(self.url).json()['features']:
                _tmp = {_key: _feat['properties'][_key] for _key in _feat['properties']
                        if _key in ['collection', 'productIdentifier', 'productIdentifier',
                                    'title', 'organisationName', 'startDate', 'completionDate',
                                    'productType', 'processingLevel', 'platform', 'instrument', 'updated',
                                    'published', 'snowCover', 'cloudCover']}
                res.append(_tmp)
        return res
