import re
import typing
import os

import numpy as np
import otbApplication

import e2o_creo_backend.processing.io as io


class Ndvi(object):
    def __init__(self):
        self.__BAND_RGXP = re.compile(pattern='.*_(?P<band>B\d+\w*)_.*')

    def calculate_index(self, files: typing.List[str]) -> typing.Any:
        bands = {'B08': None,
                 'B04': None}
        out_meta = None
        for _file in files:
            band_match = self.__BAND_RGXP.search(string=_file)
            if band_match and band_match.group('band') in bands.keys():
                rasterio_obj = io.ReadWrite.read(file_name=_file)
                bands[band_match.group('band')] = rasterio_obj.rasterio_src.read()
                if out_meta is None:
                    out_meta = rasterio_obj.rasterio_src.meta
        with np.errstate(divide='ignore', invalid='ignore'):
            ndvi = ((bands['B08'].astype(float) - bands['B04'].astype(float))
                    /
                    (bands['B08'].astype(float) + bands['B04'].astype(float)))
        out_meta["dtype"] = "float64"
        return out_meta, ndvi


class Ndmi(object):
    def __init__(self):
        self.__BAND_RGXP = re.compile(pattern='.*_(?P<band>B\d+\w*)_.*')

    def calculate_index(self, files: typing.List[str]) -> typing.Any:
        bands = {'B08': None,
                 'B11': None}
        out_meta = None
        for _file in files:
            band_match = self.__BAND_RGXP.search(string=_file)
            if band_match and band_match.group('band') in bands.keys():
                rasterio_obj = io.ReadWrite.read(file_name=_file)
                bands[band_match.group('band')] = rasterio_obj.rasterio_src.read()
                if out_meta is None:
                    out_meta = rasterio_obj.rasterio_src.meta
        with np.errstate(divide='ignore', invalid='ignore'):
            ndmi = ((bands['B08'].astype(float) - bands['B11'].astype(float))
                    /
                    (bands['B08'].astype(float) + bands['B11'].astype(float)))
        out_meta["dtype"] = "float64"
        return out_meta, ndmi


class Lai(object):
    def __init__(self):
        self.__BAND_RGXP = re.compile(pattern='.*_(?P<band>B\d+\w*)_.*')

    def calculate_index(self, files: typing.List[str]) -> typing.Any:
        bands = {'B05': None,
                 'B09': None,
                 'B12': None}
        out_meta = None
        for _file in files:
            band_match = self.__BAND_RGXP.search(string=_file)
            if band_match and band_match.group('band') in bands.keys():
                rasterio_obj = io.ReadWrite.read(file_name=_file)
                bands[band_match.group('band')] = rasterio_obj.rasterio_src.read()
                if out_meta is None:
                    out_meta = rasterio_obj.rasterio_src.meta
        with np.errstate(divide='ignore', invalid='ignore'):
            lai = ((bands['B09'].astype(float))
                   /
                   (bands['B05'].astype(float) + bands['B12'].astype(float)))
        out_meta["dtype"] = "float64"
        return out_meta, lai


class CD(object):
    @classmethod
    def calculate_change_detection(cls,
                                   reference_image_path: str,
                                   target_image_path: str,
                                   output_image_path: str) -> None:
        # Initialize OTB MAD application and configure the parameters
        os.makedirs(os.path.join('/', *output_image_path.split('/')[:-1]), exist_ok=True)
        app = otbApplication.Registry.CreateApplication("MultivariateAlterationDetector")

        app.SetParameterString("in1", reference_image_path)
        app.SetParameterString("in2", target_image_path)
        app.SetParameterString("out", output_image_path)

        try:
            app.ExecuteAndWriteOutput()

        except Exception as e:
            print(e)
            print("Input parameters are not valid! Please update them and try again.")
