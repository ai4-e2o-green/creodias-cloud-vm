import os
import typing
import re
import rasterio


class RasterioReader(object):
    def __init__(self, file_name: str, mode: str) -> None:
        self.file_name = file_name
        self.mode = mode
        self.rasterio_src = None

    def read(self) -> typing.Any:
        self.rasterio_src = rasterio.open(self.file_name, mode=self.mode)
        return self

    def __del__(self):
        if self.rasterio_src is not None:
            self.rasterio_src.close()


class ReadWrite(object):
    @classmethod
    def get_data_file_paths(
            cls, root_dir: str,
            target_dir: typing.Optional[str] = None,
            target_file_regex: typing.Optional[str] = None
    ) -> typing.Dict[str, typing.List[str]]:
        target_files = {}
        for _root, _, _files in os.walk(top=root_dir):
            for _file in _files:
                if target_dir is not None and target_dir not in _root:
                    continue
                if target_file_regex is not None and not re.match(target_file_regex, _file):
                    continue
                parent_dir = _root.split(sep='/')[-1]
                if parent_dir not in target_files:
                    target_files[parent_dir] = []
                target_files[parent_dir].append(os.path.join(_root, _file))
        return target_files

    @classmethod
    def read(cls, file_name:str) -> typing.Any:
        return RasterioReader(file_name=file_name, mode='r').read()

    @classmethod
    def write_geotif(cls,
                     out_meta: typing.Any,
                     out_image: typing.Any,
                     output_dir:str,
                     write_type: str,
                     product_identifier:str,
                     precision:str,
                     output_file_name:str) -> typing.Dict:
        output_dir_path = os.path.join(output_dir,
                                       *product_identifier.split('/')[2:],
                                       write_type,
                                       precision)
        os.makedirs(output_dir_path, exist_ok=True)
        output_file = os.path.join(output_dir_path, output_file_name)
        with rasterio.open(output_file, "w", **out_meta) as _dest:
            _dest.write(out_image)
        return {'productIdentifier': product_identifier, f'productIdentifier{write_type.capitalize()}': output_file}
