import os
import typing

import rasterio


class Stacker(object):
    @classmethod
    def stacking(cls, files: typing.List[str], stacking_output: str):
        # Read metadata of first file
        with rasterio.open(files[0]) as src0:
            meta = src0.meta
        # Update meta to reflect the number of layers
        meta.update(count=len(files))
        # Read each layer and write it to stack
        os.makedirs(os.path.join('/', *stacking_output.split('/')[:-1]), exist_ok=True)
        with rasterio.open(stacking_output, mode='w', **meta) as dst:
            for _id, _layer in enumerate(files, start=1):
                with rasterio.open(_layer) as _src1:
                    dst.write_band(_id, _src1.read(1))
