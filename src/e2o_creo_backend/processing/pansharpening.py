import re
import os
import typing

import otbApplication

import e2o_creo_backend.processing.io as io


class PanSharpening(object):
    def __init__(self):
        self.__BAND_RGXP = re.compile(pattern='.*_(?P<band>B\d+\w*)_.*')

    def pan_sharpening_band(self, files:typing.List[str]) -> typing.Any:
        bands = {}
        out_meta = None
        for _file in files:
            band_match = self.__BAND_RGXP.search(string=_file)
            if band_match:
                rasterio_obj = io.ReadWrite.read(file_name=_file)
                bands[band_match.group('band')] = rasterio_obj.rasterio_src.read()
                if out_meta is None:
                    out_meta = rasterio_obj.rasterio_src.meta

        out_meta['dtype'] = "float64"
        pansharpening_band = (bands['B02'].astype(float) +
                              bands['B03'].astype(float) +
                              bands['B08'].astype(float) +
                              bands['B04'].astype(float)) / 4
        return out_meta, pansharpening_band

    def pansharpening_precision(self,
                                files: typing.List[str],
                                pansharpening_band_path: str,
                                precision:str = 'R20m') -> typing.Any:
        pan_bands = {'R20m': ["B05", "B06", "B07", "B8A", "B11", "B12"],
                     'R60m': ["B01", "B09"]}
        superimposes = {}
        pansharpenings = {}
        pansharpening_dir = '/'.join(pansharpening_band_path.split('/')[:-1])
        os.makedirs(os.path.join(pansharpening_dir, 'superimposed', precision), exist_ok=True)
        os.makedirs(os.path.join(pansharpening_dir, 'pan_bands', precision), exist_ok=True)
        for _file in files:
            target_file_name = _file.split('/')[-1]
            band_match = self.__BAND_RGXP.search(string=_file)
            if band_match and band_match.group('band') in pan_bands[precision]:
                # The following line creates an instance of the Superimpose application
                superimpose = otbApplication.Registry.CreateApplication("Superimpose")
                # The following lines set all the application parameters:
                superimpose.SetParameterString("inr", pansharpening_band_path)
                superimpose.SetParameterString("inm", _file)
                superimpose_output_file_path = f"{pansharpening_dir}/superimposed/{precision}/{target_file_name}"
                superimpose.SetParameterString("out", superimpose_output_file_path)
                # The following line execute the application
                # Superimpose.ExecuteAndWriteOutput()
                superimposes[superimpose_output_file_path] = superimpose
                # The following line creates an instance of the Pansharpening application
                pansharpening = otbApplication.Registry.CreateApplication("Pansharpening")
                # The following lines set all the application parameters:
                pansharpening.SetParameterString("inp", pansharpening_band_path)
                pansharpening.SetParameterString("inxs", superimpose_output_file_path)
                pansharpening_output_file_path = f"{pansharpening_dir}/pan_bands/{precision}/{target_file_name}"
                pansharpening.SetParameterString("out", pansharpening_output_file_path)
                pansharpening.SetParameterOutputImagePixelType("out", 3)
                # The following line execute the application
                # Pansharpening.ExecuteAndWriteOutput()
                pansharpenings[pansharpening_output_file_path] = pansharpening
        return superimposes, pansharpenings
