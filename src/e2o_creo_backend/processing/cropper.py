import os
import typing

import pyproj
from shapely.ops import transform
from rasterio.mask import mask


class Crop(object):
    @classmethod
    def process(cls,
                rasterio_src: typing.Any,
                geom:typing.Any) -> typing.Any:
        project = pyproj.Transformer.from_crs(pyproj.CRS('EPSG:4326'), rasterio_src.crs.data, always_xy=True).transform
        utm_polygon = transform(project, geom)
        out_image, out_transform = mask(rasterio_src, [utm_polygon], crop=True)

        out_meta = rasterio_src.meta
        out_meta.update({"driver": "GTiff",
                         "height": out_image.shape[1],
                         "width": out_image.shape[2],
                         "transform": out_transform,
                         "nodata": 65000
                         })
        return out_meta, out_image
