import typing
from collections.abc import Iterable

from sqlalchemy import create_engine, select
from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Numeric
from geoalchemy2 import Geometry
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.inspection import inspect


Base = declarative_base()


class ModelUtils(object):
    def as_dict(self, without_prime: bool = False) -> typing.Mapping:
        exclude = ['_sa_instance_state']
        if without_prime:
            exclude.append(self.get_primary_key_name())
        _d = vars(self)
        return {_key: _d[_key] for _key in _d if _key not in exclude}

    @classmethod
    def get_primary_key_name(cls):
        return inspect(cls).primary_key[0].name


class EODataIndex(ModelUtils, Base):
    __tablename__ = 'eodata_index'
    __table_args__ = {'extend_existing': True}
    productIdentifier = Column(String, primary_key=True)
    title = Column(String)
    organisationName = Column(String)
    collection = Column(String)
    startDate = Column(DateTime)
    completionDate = Column(DateTime)
    productType = Column(String)
    processingLevel = Column(String)
    platform  = Column(String)
    instrument = Column(String)
    updated = Column(DateTime)
    published = Column(DateTime)
    snowCover = Column(Numeric)
    cloudCover = Column(Numeric)
    requested_polygon = Column(Geometry)


class EODataCropped(ModelUtils, Base):
    __tablename__ = 'eodata_cropped'
    __table_args__ = {'extend_existing': True}
    productIdentifierCropped = Column(String, primary_key=True)
    productIdentifier = Column(String)


class EODataPanSharpening(ModelUtils, Base):
    __tablename__ = 'eodata_pansharpening'
    __table_args__ = {'extend_existing': True}
    productIdentifierPansharpening = Column(String, primary_key=True)
    productIdentifierType = Column(String)
    productIdentifier = Column(String)
    precision = Column(String)


class EODataNdvi(ModelUtils, Base):
    __tablename__ = 'eodata_ndvi'
    __table_args__ = {'extend_existing': True}
    productIdentifierNdvi = Column(String, primary_key=True)
    productIdentifier = Column(String)


class EODataNdmi(ModelUtils, Base):
    __tablename__ = 'eodata_ndmi'
    __table_args__ = {'extend_existing': True}
    productIdentifierNdmi = Column(String, primary_key=True)
    productIdentifier = Column(String)


class EODataLai(ModelUtils, Base):
    __tablename__ = 'eodata_lai'
    __table_args__ = {'extend_existing': True}
    productIdentifierLai = Column(String, primary_key=True)
    productIdentifier = Column(String)


class EODataStacked(ModelUtils, Base):
    __tablename__ = 'eodata_stacked'
    __table_args__ = {'extend_existing': True}
    productIdentifier = Column(String, primary_key=True)
    productIdentifierStacked = Column(String)


class EODataCD(ModelUtils, Base):
    __tablename__ = 'eodata_cd'
    __table_args__ = {'extend_existing': True}
    productIdentifier = Column(String, primary_key=True)
    productIdentifierReferent = Column(String)
    productIdentifierTarget = Column(String)
    productIdentifierCD = Column(String)


class DBClient(object):
    def __init__(self, user: str, password: str, db: str, host: str, port: int = 5432) -> None:
        engine = create_engine(f'postgresql://{user}:{password}@{host}:{port}/{db}', echo=True)
        Base.metadata.create_all(engine)
        self._Session = sessionmaker(bind=engine)

    def __enter__(self):
        self.session = self._Session()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            print(exc_type)
        if exc_val:
            print(exc_val)
        if exc_tb:
            print(exc_tb)
        self.session.close()

    @classmethod
    def upsert(cls, session: typing.Any, model: typing.Any, row: typing.Any) -> None:
        statement = insert(model).values(**row.as_dict()).on_conflict_do_update(index_elements=[row.get_primary_key_name()],
                                                                                set_=row.as_dict(without_prime=True))
        session.execute(statement)
        session.commit()

    @classmethod
    def select(cls, session: typing.Any,
               model: typing.Any,
               where: typing.Optional[typing.Any] = None,
               filter: typing.Optional[typing.Union[typing.Iterable, typing.Any]] = None,
               return_statement_only: bool = False) -> typing.Any:
        statement = select(model)
        if where is not None:
            statement = statement.where(where)
        if filter is not None:
            if not isinstance(filter, Iterable):
                filter = [filter]
            for _f in filter:
                statement = statement.filter(_f)
        if return_statement_only:
            return statement
        return session.execute(statement)
