import typing

import yaml


def read_yaml(file_path: str) -> typing.Mapping:
    try:
        with open(file=file_path, mode='r') as _f:
            return yaml.safe_load(_f.read())
    except yaml.YAMLError as e:
        print(e)
        return {}
