# e2o-creo-backend

This project is intended for use in the Creodias public cloud (called WAW3-1) with mounted EO cloud data.

This project consists of a python package that can be used without docker services or with a predefined docker services configuration.

## Using predefined docker services

To use predefined docker services do:
1. Run Postgis service like:
  ```bash 
  sudo docker-compose -f docker/postgis/docker-compose.yml up --build -d
  ```
2. Build Airflow image with Orfeo Toolbox
  ```bash
  pushd docker/airflow/airflow_custom_image/
  sudo DOCKER_BUILDKIT=1 docker build . --tag 'airflow-with-otbapplication:0.0.1' 
  popd
  ```
3. Run Airflow service like:
  ```bash
  pushd docker/airflow/
  sudo docker-compose up --build -d
  popd
  ```
4. Directory `docker/airflow/dags` consists of predefined dags and tasks for EO data orchestration. Feel free to change it or slightly modified to fulfill your requests.

* Creodias public VM IP `64.225.133.59`
* Postgis instance: `64.225.133.59:5432`
* PGAdmin instance: `64.225.133.59:5050`
* Airflow instance: `64.225.133.59:8080`

## Predefined Airflow pipeline

![Predefined Airflow pipeline](docs/predefined_airflow_pipeline.png)
