from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

import pendulum

import os
import e2o_creo_backend.db.models as models
import e2o_creo_backend.processing.indices as indices
import e2o_creo_backend.processing.stacker as stacker
import collections


CDTuple = collections.namedtuple(typename='CDTuple', field_names=['reference', 'target'])

OUTPUT_ROOT_DIR = '/processed'


def _store_metadata(data, model):
    # TODO: hide credentials
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
            models.DBClient.upsert(session=session, model=model, row=data)


def _get_cd_candidate_data():
    eodata_cd_rows = []
    # TODO: hide credentialse
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        query_eodata_cd = models.DBClient.select(
            session=session,
            model=models.EODataCD.productIdentifier,
            return_statement_only=True
        )
        query_eodata_index = models.DBClient.select(
            session=session,
            model=models.EODataIndex,
            filter=[~models.EODataIndex.productIdentifier.in_(query_eodata_cd), models.EODataIndex.platform == 'S2A',
                    models.EODataIndex.processingLevel == 'LEVEL2A', models.EODataIndex.instrument == 'MSI',
                    models.EODataIndex.cloudCover <= 15.0]
        )
        for _row in query_eodata_index.scalars():
            _tmp = _row.as_dict()
            eodata_cd_rows.append(_tmp)

    return eodata_cd_rows


def _get_paths_for_stacking(product_identifier):
    eodata_for_stacking = []
    # TODO: hide credentialse
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        query_eodata_ndvi = models.DBClient.select(
            session=session,
            model=models.EODataNdvi.productIdentifierNdvi,
            filter=[models.EODataNdvi.productIdentifier == product_identifier]
        )
        eodata_for_stacking.append(query_eodata_ndvi.first()[0])
        query_eodata_ndmi = models.DBClient.select(
            session=session,
            model=models.EODataNdmi.productIdentifierNdmi,
            filter=[models.EODataNdmi.productIdentifier == product_identifier]
        )
        eodata_for_stacking.append(query_eodata_ndmi.first()[0])
        query_eodata_lai = models.DBClient.select(
            session=session,
            model=models.EODataLai.productIdentifierLai,
            filter=[models.EODataLai.productIdentifier == product_identifier]
        )
        eodata_for_stacking.append(query_eodata_lai.first()[0])
    return eodata_for_stacking


def _driver():
    cd_candidates = _get_cd_candidate_data()
    cd_candidates = sorted(cd_candidates, key=lambda x: x['startDate'])
    ref_tar = []
    for _index in range(len(cd_candidates) - 1):
        ref_tar.append(CDTuple(reference=cd_candidates[_index]['productIdentifier'],
                               target=cd_candidates[_index + 1]['productIdentifier']))
    for _ref_tar in ref_tar:
        _ref_stack_paths = _get_paths_for_stacking(product_identifier=_ref_tar.reference)
        _tar_stack_paths = _get_paths_for_stacking(product_identifier=_ref_tar.target)
        if len(_ref_stack_paths) < 3 or len(_tar_stack_paths) < 3:
            continue
        # stack reference
        ref_stacking_output = os.path.join(
            OUTPUT_ROOT_DIR, *_ref_tar.reference.split('/')[2:], 'stacked_raster', 'stacked.tif'
        )
        stacker.Stacker.stacking(
            files=_ref_stack_paths,
            stacking_output=ref_stacking_output
        )
        _store_metadata(data=models.EODataStacked(productIdentifier=_ref_tar.reference,
                                                  productIdentifierStacked=ref_stacking_output),
                        model=models.EODataStacked)
        # stack target
        tar_stacking_output = os.path.join(
            OUTPUT_ROOT_DIR, *_ref_tar.target.split('/')[2:], 'stacked_raster', 'stacked.tif'
        )
        stacker.Stacker.stacking(
            files=_tar_stack_paths,
            stacking_output=tar_stacking_output
        )
        _store_metadata(data=models.EODataStacked(productIdentifier=_ref_tar.target,
                                                  productIdentifierStacked=tar_stacking_output),
                        model=models.EODataStacked)
        output_image_path = os.path.join(
            OUTPUT_ROOT_DIR, *_ref_tar.target.split('/')[2:], 'cd', 'cd.tif'
        )
        indices.CD.calculate_change_detection(
            reference_image_path=ref_stacking_output,
            target_image_path=tar_stacking_output,
            output_image_path=output_image_path
        )
        _store_metadata(
            data=models.EODataCD(
                productIdentifier = _ref_tar.target,
                productIdentifierReferent = ref_stacking_output,
                productIdentifierTarget = tar_stacking_output,
                productIdentifierCD = output_image_path
            ),
            model=models.EODataCD
        )



with DAG("eodata_change_detection",  # Dag id
         start_date=pendulum.datetime(2023, 1, 1),  # start date, the 1st of January 2023
         schedule_interval=None,  # Cron expression, here it is a preset of Airflow, @daily means once every day.
         catchup=True  # Catchup
         ) as dag:
    eo_data_cahnge_detection_index_task = PythonOperator(
        task_id="eo_data_change_detection_index",
        python_callable=_driver
    )
    eo_data_cahnge_detection_index_task
