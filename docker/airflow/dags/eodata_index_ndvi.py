from airflow import DAG
from airflow.operators.python import PythonOperator
import re

import pendulum

import e2o_creo_backend.processing.indices as indices
import e2o_creo_backend.processing.io as io
import e2o_creo_backend.db.models as models


OUTPUT_DIR = '/processed'


def _store_ndvi_metadata(data):
    # TODO: hide credentials
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
            models.DBClient.upsert(session=session, model=models.EODataNdvi, row=data)


def _get_ndvi_candidate_data():
    eodata_ndvi_rows = []
    # TODO: hide credentialse
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        query_eodata_ndvi = models.DBClient.select(
            session=session,
            model=models.EODataNdvi.productIdentifier,
            return_statement_only=True
        )
        query_eodata_cropped = models.DBClient.select(
            session=session,
            model=models.EODataCropped,
            filter=~models.EODataCropped.productIdentifier.in_(query_eodata_ndvi)
        )
        for _row in query_eodata_cropped.scalars():
            _tmp = _row.as_dict()
            eodata_ndvi_rows.append(_tmp)

    return eodata_ndvi_rows


def _driver():
    precision_rgxp = re.compile(pattern=r'.*/cropped/(?P<precision>R\d+m)/.*')
    tmp_candidates = _get_ndvi_candidate_data()
    candidates = {}
    for _tcand in tmp_candidates:
        if _tcand['productIdentifier'] not in candidates:
            candidates[_tcand['productIdentifier']] = {}
        m = precision_rgxp.search(string=_tcand['productIdentifierCropped'])
        if m:
            precision = m.group('precision')
            if precision != 'R10m':
                continue
            if precision not in candidates[_tcand['productIdentifier']]:
                candidates[_tcand['productIdentifier']][precision] = []
            candidates[_tcand['productIdentifier']][precision].append(_tcand['productIdentifierCropped'])

    ndvi_obj = indices.Ndvi()
    for _product_identifier in candidates:
        out_meta, ndvi = ndvi_obj.calculate_index(
            files=candidates[_product_identifier]['R10m']
        )
        ndvi_data = io.ReadWrite.write_geotif(
            out_meta=out_meta,
            out_image=ndvi,
            output_dir=OUTPUT_DIR,
            write_type='ndvi',
            product_identifier=_product_identifier,
            precision='',
            output_file_name="ndvi.tif"
        )
        _store_ndvi_metadata(data=models.EODataNdvi(**ndvi_data))


with DAG("eodata_index_ndvi",  # Dag id
         start_date=pendulum.datetime(2023, 1, 1),  # start date, the 1st of January 2021
         schedule_interval=None,
         catchup=False  # Catchup
         ) as dag:
    eo_data_ndvi_task = PythonOperator(
        task_id="eo_data_ndvi",
        python_callable=_driver
    )
