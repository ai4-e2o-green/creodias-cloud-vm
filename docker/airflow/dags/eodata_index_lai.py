from airflow import DAG
from airflow.operators.python import PythonOperator
import re

import pendulum

import e2o_creo_backend.processing.indices as indices
import e2o_creo_backend.processing.io as io
import e2o_creo_backend.db.models as models


OUTPUT_DIR = '/processed'


def _store_lai_metadata(data):
    # TODO: hide credentials
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
            models.DBClient.upsert(session=session, model=models.EODataLai, row=data)


def _get_lai_candidate_data():
    eodata_lai_rows = []
    # TODO: hide credentialse
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        query_eodata_lai = models.DBClient.select(
            session=session,
            model=models.EODataLai.productIdentifier,
            return_statement_only=True
        )
        query_eodata_pansharpening = models.DBClient.select(
            session=session,
            model=models.EODataPanSharpening,
            filter=~models.EODataPanSharpening.productIdentifier.in_(query_eodata_lai)
        )
        for _row in query_eodata_pansharpening.scalars():
            _tmp = _row.as_dict()
            eodata_lai_rows.append(_tmp)

    return eodata_lai_rows


def _driver():
    precision_panshaped_rgxp = re.compile(pattern=r'.*/pansharpening/pan_bands/(?P<precision>R\d+m)/.*')
    tmp_candidates = _get_lai_candidate_data()
    candidates = {}
    for _tcand in tmp_candidates:
        if _tcand['productIdentifier'] not in candidates:
            candidates[_tcand['productIdentifier']] = {}
        m = precision_panshaped_rgxp.search(string=_tcand['productIdentifierPansharpening'])
        if m:
            precision = m.group('precision')
            if precision not in ['R20m', 'R60m']:
                continue
            if precision not in candidates[_tcand['productIdentifier']]:
                candidates[_tcand['productIdentifier']][precision] = []
            candidates[_tcand['productIdentifier']][precision].append(_tcand['productIdentifierPansharpening'])

    lai_obj = indices.Lai()
    for _product_identifier in candidates:
        out_meta, lai = lai_obj.calculate_index(
            files=[*candidates[_product_identifier]['R20m'], *candidates[_product_identifier]['R60m']]
        )
        lai_data = io.ReadWrite.write_geotif(
            out_meta=out_meta,
            out_image=lai,
            output_dir=OUTPUT_DIR,
            write_type='lai',
            product_identifier=_product_identifier,
            precision='',
            output_file_name="lai.tif"
        )
        _store_lai_metadata(data=models.EODataLai(**lai_data))


with DAG("eodata_index_lai",  # Dag id
         start_date=pendulum.datetime(2023, 1, 1),  # start date, the 1st of January 2021
         schedule_interval=None,
         catchup=False  # Catchup
         ) as dag:
    eo_data_lai_task = PythonOperator(
        task_id="eo_data_lai",
        python_callable=_driver
    )
