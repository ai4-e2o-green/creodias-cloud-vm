from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

import datetime
import pendulum
import importlib.resources

import e2o_creo_backend.source.creofinder as creofinder
import e2o_creo_backend.utils.readers as readers
import e2o_creo_backend.db.models as models


def _store_index(start_date):
    with importlib.resources.path('e2o_creo_backend', 'resources') as data_path:
        default_config_path = data_path / "sources.yml"
        contents = readers.read_yaml(file_path=str(default_config_path))

    start_date = (datetime.datetime.strptime(start_date, '%Y-%m-%dT%H:%M:%S%z') + datetime.timedelta(days=-1)).strftime("%Y-%m-%dT%H:%M:%S")
    table_result = []
    for _col in contents['collections']:
        for _prop in contents['collections'][_col]:
            data = creofinder.DataIndex(
                collection=_col, platform=_prop['platform'], processing_level=_prop['processing_level'],
                geometry=contents['geometry'], start_date=start_date
            )
            res = data.get_metadata()
            table_result.extend([models.EODataIndex(requested_polygon=contents['geometry'], **_r) for _r in res])
    # TODO: hide credentials
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        for _item in table_result:
            models.DBClient.upsert(session=session, model=models.EODataIndex, row=_item)


with DAG("db_satelite_data_index_populate", # Dag id
         start_date=pendulum.datetime(2021, 1 ,1), # start date, the 1st of January 2023
         schedule_interval='@daily',  # Cron expression, here it is a preset of Airflow, @daily means once every day.
         catchup=True,  # Catchup,
         max_active_runs=1
     ) as dag:
     eo_data_source_index_task = PythonOperator(
                                     task_id="eo_data_source_index",
                                     op_kwargs={'start_date': '{{ ts }}'},
                                     python_callable=_store_index,

                                 )
     trigger_crop_dag_task = TriggerDagRunOperator(
                                 task_id="trigger_cropp_dag",
                                 trigger_dag_id="eodata_index_crop",
                                 wait_for_completion=True
                             )
     eo_data_source_index_task >> trigger_crop_dag_task
