from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

import re

import pendulum

import e2o_creo_backend.processing.pansharpening as pansharpening
import e2o_creo_backend.processing.io as io
import e2o_creo_backend.db.models as models


OUTPUT_DIR = '/processed'


def _store_pansharpening_metadata(data):
    # TODO: hide credentials
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
            models.DBClient.upsert(session=session, model=models.EODataPanSharpening, row=data)


def _get_pansharping_candidate_data():
    eodata_pansharpening_rows = []
    # TODO: hide credentialse
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        query_eodata_pansharpening = models.DBClient.select(
            session=session,
            model=models.EODataPanSharpening.productIdentifier,
            return_statement_only=True
        )
        query_eodata_cropped = models.DBClient.select(
            session=session,
            model=models.EODataCropped,
            filter=~models.EODataCropped.productIdentifier.in_(query_eodata_pansharpening)
        )
        for _row in query_eodata_cropped.scalars():
            _tmp = _row.as_dict()
            eodata_pansharpening_rows.append(_tmp)

    return eodata_pansharpening_rows


def _driver():
    precision_rgxp = re.compile(pattern=r'.*/cropped/(?P<precision>R\d+m)/.*')
    tmp_candidates = _get_pansharping_candidate_data()
    candidates = {}
    for _tcand in tmp_candidates:
        if _tcand['productIdentifier'] not in candidates:
            candidates[_tcand['productIdentifier']] = {}
        m = precision_rgxp.search(string=_tcand['productIdentifierCropped'])
        if m:
            precision = m.group('precision')
            if precision not in candidates[_tcand['productIdentifier']]:
                candidates[_tcand['productIdentifier']][precision] = []
            candidates[_tcand['productIdentifier']][precision].append(_tcand['productIdentifierCropped'])

    pansharpening_obj = pansharpening.PanSharpening()
    for _product_identifier in candidates:
        out_meta, pansharpening_band = pansharpening_obj.pan_sharpening_band(
            files=candidates[_product_identifier]['R10m']
        )
        pansharpening_band_data = io.ReadWrite.write_geotif(
            out_meta=out_meta,
            out_image=pansharpening_band,
            output_dir=OUTPUT_DIR,
            write_type='pansharpening',
            product_identifier=_product_identifier,
            precision='',
            output_file_name=f"pansharpening_band.tif"
        )
        pansharpening_band_data['productIdentifierType'] = 'base'
        pansharpening_band_data['precision'] = 'R10m'
        _store_pansharpening_metadata(data=models.EODataPanSharpening(**pansharpening_band_data))

        for _precision in ['R20m', 'R60m']:
            superimposes, pansharpenings = pansharpening_obj.pansharpening_precision(
                files=candidates[_product_identifier][_precision],
                pansharpening_band_path=pansharpening_band_data['productIdentifierPansharpening'],
                precision=_precision)
            for _sfile, _pfile in zip(superimposes, pansharpenings):
                superimposes[_sfile].ExecuteAndWriteOutput()
                data = {'productIdentifierType': 'superimpose',
                        'productIdentifier': pansharpening_band_data['productIdentifier'],
                        'productIdentifierPansharpening': _sfile,
                        'precision': _precision}
                _store_pansharpening_metadata(data=models.EODataPanSharpening(**data))
                pansharpenings[_pfile].ExecuteAndWriteOutput()
                data = {'productIdentifierType': 'pansharpening',
                        'productIdentifier': pansharpening_band_data['productIdentifier'],
                        'productIdentifierPansharpening': _pfile,
                        'precision': _precision}
                _store_pansharpening_metadata(data=models.EODataPanSharpening(**data))


with DAG("eodata_index_pansharpening", # Dag id
         start_date=pendulum.datetime(2023, 1 ,1), # start date, the 1st of January 2021
         schedule_interval=None,
         catchup=False  # Catchup
     ) as dag:
     eo_data_pansharpening_task = PythonOperator(
         task_id="eo_data_pansharpening",
         python_callable=_driver
     )
     trigger_ndmi_dag_task = TriggerDagRunOperator(
         task_id="trigger_ndmi_dag",
         trigger_dag_id="eodata_index_ndmi",
         wait_for_completion=True
     )
     trigger_lai_dag_task = TriggerDagRunOperator(
         task_id="trigger_lai_dag",
         trigger_dag_id="eodata_index_lai",
         wait_for_completion=True
     )
     eo_data_pansharpening_task >> [trigger_ndmi_dag_task, trigger_lai_dag_task]
