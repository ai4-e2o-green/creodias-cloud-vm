from airflow import DAG
from airflow.operators.bash import BashOperator

import pendulum

with DAG("log_cleanup",  # Dag id
         start_date=pendulum.datetime(2023, 1, 1),  # start date, the 1st of January 2023
         schedule_interval='0 0 * * *',
         catchup=True  # Catchup
         ) as dag:
    cleanup_task = BashOperator(
        task_id="cleanup",
        bash_command="find /opt/airflow/logs/ -name '*.log' -type f -mtime +7 -delete"
    )
    cleanup_task
