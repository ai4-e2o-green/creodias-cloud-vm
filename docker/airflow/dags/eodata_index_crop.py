from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

import pendulum

import e2o_creo_backend.db.models as models
import os
from geoalchemy2.shape import to_shape
import e2o_creo_backend.processing.io as io
import e2o_creo_backend.processing.cropper as cropper
import e2o_creo_backend.db.models as models


OUTPUT_DIR = '/processed'


def _store_cropped_metadata(data):
    # TODO: hide credentials
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
            models.DBClient.upsert(session=session, model=models.EODataCropped, row=data)


def _get_crop_candidate_data():
    eodata_index_rows = []
    # TODO: hide credentialse
    with models.DBClient(user='postgres', password='0605DAPostgis', db='gis', host='64.225.133.59') as session:
        query_eodata_cropped = models.DBClient.select(session=session,
                                                      model=models.EODataCropped.productIdentifier,
                                                      return_statement_only=True)
        query_eodata_index = models.DBClient.select(session=session,
                                                    model=models.EODataIndex,
                                                    filter=~models.EODataIndex.productIdentifier.in_(query_eodata_cropped))
        for _row in query_eodata_index.scalars():
            _tmp = _row.as_dict()
            _tmp["requested_polygon"] = to_shape(_tmp["requested_polygon"])
            eodata_index_rows.append(_tmp)

    return eodata_index_rows


def _driver():
    eodata_index_candidates = _get_crop_candidate_data()
    for _row in eodata_index_candidates:
        files_map = io.ReadWrite.get_data_file_paths(root_dir=_row['productIdentifier'],
                                                     target_dir='IMG_DATA',
                                                     target_file_regex=r'.*_B\d+\w*_\d+m\.jp2')
        for _band_precision in files_map:
            for _file in files_map[_band_precision]:
                rasterio_obj = io.ReadWrite.read(file_name=_file)
                out_meta, out_image = cropper.Crop.process(rasterio_src=rasterio_obj.rasterio_src,
                                                           geom=_row["requested_polygon"])
                cropped_data = io.ReadWrite.write_geotif(
                    out_meta=out_meta,
                    out_image=out_image,
                    output_dir=OUTPUT_DIR,
                    write_type='cropped',
                    product_identifier=_row['productIdentifier'],
                    precision=_band_precision,
                    output_file_name=f"{'.'.join(_file.split('/')[-1].split('.')[:-1])}.tif"
                )
                _store_cropped_metadata(data=models.EODataCropped(**cropped_data))


with DAG("eodata_index_crop", # Dag id
         start_date=pendulum.datetime(2023, 1 ,1), # start date, the 1st of January 2021
         schedule_interval=None,
         catchup=False  # Catchup
     ) as dag:
    eo_data_crop_task = PythonOperator(
        task_id="eo_data_crop",
        python_callable=_driver
    )
    trigger_pansharpening_dag_task = TriggerDagRunOperator(
        task_id="trigger_pansharpening_dag",
        trigger_dag_id="eodata_index_pansharpening",
        wait_for_completion=True
    )
    trigger_ndvi_dag_task = TriggerDagRunOperator(
        task_id="trigger_ndvi_dag",
        trigger_dag_id="eodata_index_ndvi",
        wait_for_completion=True
    )
    trigger_cd_dag_task = TriggerDagRunOperator(
        task_id="trigger_cd_dag",
        trigger_dag_id="eodata_change_detection",
        wait_for_completion=True
    )
    eo_data_crop_task >> [trigger_pansharpening_dag_task, trigger_ndvi_dag_task] >> trigger_cd_dag_task
